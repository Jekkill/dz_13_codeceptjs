const { setHeadlessWhen } = require('@codeceptjs/configure');

// turn on headless mode when running with HEADLESS=true environment variable
// export HEADLESS=true && npx codeceptjs run
setHeadlessWhen(process.env.HEADLESS);

exports.config = {
  tests: './specs/*.spec.js',
  output: './output',
  helpers: {
    Playwright: {
      url: 'https://idemo.bspb.ru',
      show: false,
      browser: 'chromium',
    }
  },
  include: {
    I: './steps_file.js',
    authPage: './framework/pages/auth.js',
    accountPage: './framework/pages/account.js',
    menuFragment: './framework/fragments/menu.js'
  },
  bootstrap: null,
  mocha: {
    reporterOptions: {
      reportDir: 'output'
    }
  },
  name: '13',
  plugins: {
    autoLogin: {
      enabled: true,
      saveToFile: true,
      inject: 'login',
      users: {
        user: {
          login: (I) => I.login(),
          check: (I) => {
            I.amOnPage('/');
            I.see('Королёва Ольга');
          },
          fetch: () => {},
          restore: () => {}
        }
      }
    },
    pauseOnFail: {},
    retryFailedStep: {
      enabled: true
    },
    tryTo: {
      enabled: true
    },
    screenshotOnFail: {
      enabled: true
    }
  },
};