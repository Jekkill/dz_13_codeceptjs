const faker = require('faker/locale/ru');
const chai = require('chai');

Feature('Account');

Before(  ({ I })=> {
    I.login();
});

Scenario('should see the account info', async ({ I }) => {
    I.amOnPage('/overview');
    I.see('Вклады');
    I.see('Счета');
    I.see('Кредиты');
});

Scenario('should change the name of account', async ({ I, accountPage }) => {
   I.amOnPage('/overview');
   const newAccountName = faker.finance.accountName();
   await accountPage.renameAccount(newAccountName);
   I.wait(2);
   const accountName = await accountPage.getAccountName();
   chai.expect(accountName).to.equal(newAccountName);
});

Scenario('should open and close the account details', async ({ I, accountPage }) => {
    I.amOnPage('/overview');
    await accountPage.openAccountDetails();
    I.see('Реквизиты для безналичного перечисления');
    await accountPage.closeAccountDetails();
    I.dontSee('Реквизиты для безналичного перечисления');
});

Scenario('should add new event', async ({ I, accountPage }) => {
    I.amOnPage('/overview');
    I.scrollTo('#event-time-line');
    const oldCountOfEvents = await accountPage.getNumberOfEvents();
    const newEventName = faker.lorem.word();
    const newEventDescription = faker.lorem.words();
    accountPage.addNewEvent(newEventName, newEventDescription);
    I.see('Событие успешно сохранено');
    I.wait(5);
    I.amOnPage('/overview');
    I.scrollTo('#event-time-line');
    const newCountOfEvents = await accountPage.getNumberOfEvents();
    chai.expect(newCountOfEvents).to.equal(oldCountOfEvents + 1);
});

Scenario('should edit event', async ({ I, accountPage }) => {
    I.amOnPage('/overview');
    I.scrollTo('#event-time-line');
    const newEventName = faker.lorem.word();
    accountPage.editEvent(newEventName);
    I.see('Событие успешно сохранено');
    I.wait(5);
    I.amOnPage('/overview');
    I.scrollTo('#event-time-line');
    const eventName = await accountPage.getEventName();
    chai.expect(newEventName).to.equal(eventName);
});

Scenario('should remove personal event', async ({ I, accountPage }) => {
    I.amOnPage('/overview');
    I.scrollTo('#event-time-line');
    const oldCountOfEvents = await accountPage.getNumberOfEvents();
    accountPage.removeEvent();
    I.see('Персональное событие удалено');
    I.wait(5);
    I.amOnPage('/overview');
    I.scrollTo('#event-time-line');
    const newCountOfEvents = await accountPage.getNumberOfEvents();
    chai.expect(newCountOfEvents).to.equal(oldCountOfEvents - 1);
});