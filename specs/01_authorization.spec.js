const user = require('../framework/config/user');

Feature('Authorization');

Scenario('should regain access', ({ I, authPage }) => {
    I.amOnPage('/');
    authPage.regainAccess();
    I.see('Забыли логин или пароль?');
    I.click('.modal-header .close');
});

Scenario('should open new page with internet bank information', ({ I, authPage }) => {
    authPage.openInfoPage();
    I.switchToNextTab();
    I.seeInCurrentUrl('retail/ibank');
    I.switchToPreviousTab();
});

Scenario('should change language', ({ I, authPage }) => {
    I.amOnPage('/');
    authPage.changeLanguage();
    I.see('About Internet-bank');
    authPage.changeLanguage();
});

Scenario('should fail login', ({ I, authPage }) => {
    I.amOnPage('/');
    authPage.manualLogin(user.validLogin, user.invalidPassword);
    I.see('Неверные данные пользователя');
});

Scenario('should successfully login', ({ I, authPage }) => {
    I.amOnPage('/');
    authPage.manualLogin(user.validLogin, user.validPassword);
    I.see('был отправлен код подтверждения, введите его для входа', '#otp-code-text');
    authPage.sendMobileCode(user.otpCode);
    I.see('Королёва Ольга');
});