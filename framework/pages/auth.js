const { I } = inject();

module.exports = {

    fields: {
        username: 'input[name="username"]',
        password: 'input[name="password"]',
        mobileCode: '#otp-code'
    },

    loginButton: '#login-button',
    loginOtpButton: '#login-otp-button',
    regainAccessButton: '#additional-actions .chevron',
    changeLanguageButton: '.secondary-links .locale',
    openInfoPageButton: '.secondary-links .chevron:nth-child(2)',

    manualLogin(username, password)
    {
        I.fillField(this.fields.username, username);
        I.fillField(this.fields.password, password);
        I.click(this.loginButton);
    },

    sendMobileCode(code)
    {
        I.fillField(this.fields.mobileCode, code);
        I.click(this.loginOtpButton);
    },

    regainAccess()
    {
        I.click(this.regainAccessButton);
    },

    changeLanguage()
    {
        I.click(this.changeLanguageButton);
    },

    openInfoPage()
    {
        I.click(this.openInfoPageButton);
    }

};
