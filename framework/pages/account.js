const { I } = inject();

module.exports = {

    fields: {
        depositAccountField: '#deposits-container .total .amount',
        renameAccountField: '.account-alias-edit-form .input-medium',
        newEventDateField: '#custom-event-form input[name="event.date"]',
        newEventNameField: '#custom-event-form div[data-name="event.name"] input',
        newEventDescriptionField: '#custom-event-form textarea[name="event.description"]',
    },

    renameAccountButton: '#accounts tbody tr:first-child .account .icon-edit',
    saveRenameAccountButton: '.account-alias-edit-form .btn-primary',
    accountNameButton: '#accounts tbody tr:first-child .account .alias',
    openAccountDetailsButton: '#accounts tbody tr:first-child .account .icon-share',
    closeAccountDetailsButton: '#requisites-popup .modal-header .close',
    addNewEventButton: '#event-time-line .event-add',
    saveNewEventButton: '#custom-event-form .modal-footer .btn-primary',
    personalEventContainer: '#event-time-line .calendar-event.event-personal',
    personalEventForEditAndDelete: '#event-time-line > .time-line-container > .event-personal',
    personalEventText: '#event-time-line > .time-line-container > .event-personal .event-type',
    saveChangesToEventButton: '#custom-event-form .modal-footer .btn-primary',
    personalEventDeleteButton: '#custom-event-form .modal-footer .post',
    confirmDeleteButton: '.confirmation-modal .modal-footer .confirm',
    accountFieldNameContainer: '#accounts tbody tr:first-child .account .alias',

    async getDepositSum()
    {
        const sum = await I.grabTextFrom(this.fields.depositAccountField);
        return sum.substr(0, sum.length - 2);
    },

    async renameAccount(newName)
    {
        I.moveCursorTo(this.accountNameButton);
        I.click(this.renameAccountButton);
        I.fillField(this.fields.renameAccountField, newName);
        I.click(this.saveRenameAccountButton);
    },

    openAccountDetails() {
        I.moveCursorTo(this.accountNameButton);
        I.click(this.openAccountDetailsButton);
    },

    closeAccountDetails()
    {
        I.click(this.closeAccountDetailsButton);
    },

    addNewEvent(name, description)
    {
        I.click(this.addNewEventButton);
        I.wait(1);
        I.fillField(this.fields.newEventNameField, name);
        I.fillField(this.fields.newEventDescriptionField, description);
        I.click(this.saveNewEventButton);
    },

    editEvent(newName)
    {
        I.click(this.personalEventForEditAndDelete);
        I.wait(1);
        I.fillField(this.fields.newEventNameField, newName);
        I.click(this.saveChangesToEventButton);
    },

    removeEvent()
    {
        I.click(this.personalEventForEditAndDelete);
        I.click(this.personalEventDeleteButton);
        I.click(this.confirmDeleteButton);
    },

    async getNumberOfEvents()
    {
        return await I.grabNumberOfVisibleElements(this.personalEventContainer);
    },

    async getAccountName()
    {
        return await I.grabTextFrom(this.accountFieldNameContainer);
    },

    async getEventName()
    {
        return await I.grabTextFrom(this.personalEventText);
    }


};
