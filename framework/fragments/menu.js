const { I } = inject();

module.exports = {

    fields: {

    },

    goToBankOverviewButton: '#bank-overview',

    goToBankOverview()
    {
        I.click(this.goToBankOverviewButton);
    }

};
