// in this file you can append custom step methods to 'I' object
const user = require('./framework/config/user');

module.exports = function() {
  return actor({

    // Define custom steps here, use 'this' to access default methods of I.
    // It is recommended to place a general 'login' function here.
      login() {
        this.amOnPage('/');
        this.fillField('input[name="username"]', user.validLogin);
        this.fillField('input[name="password"]', user.validPassword);
        this.click('#login-button');
        this.fillField('#otp-code', user.otpCode);
        this.click('#login-otp-button');
      }
  });
}
